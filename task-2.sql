/**
* Задание 2.
*
* Нарисуйте в свободной форме схему следующей БД:
* 1) есть ученики, учителя и классы
* 2) каждый ученик учится в каком-то классе
* 3) учитель может преподавать в одном или более классах
* 4) в одном классе может преподавать один или более учителей
*/
CREATE TABLE `student`(
	`id` BIGINT( 22 ) UNSIGNED NOT null AUTO_INCREMENT COMMENT 'идентификатор' ,
	`class_id` BIGINT( 22 ) UNSIGNED NOT null COMMENT 'идентификатор класса' ,

	PRIMARY KEY( `id` ) ,

	CONSTRAINT `fk_student_class_student` FOREIGN KEY ( `class_id` ) REFERENCES `class`( `id` ) ON DELETE CASCADE ON UPDATE CASCADE
) COMMENT 'ученик' ;

CREATE TABLE `teacher`(
	`id` BIGINT( 22 ) UNSIGNED NOT null AUTO_INCREMENT COMMENT 'идентификатор' ,

	PRIMARY KEY( `id` )
) COMMENT 'учитель' ;

CREATE TABLE `class`(
	`id` BIGINT( 22 ) UNSIGNED NOT null AUTO_INCREMENT COMMENT 'идентификатор' ,

	PRIMARY KEY( `id` )
) COMMENT 'класс' ;

CREATE TABLE `class_teacher`(
	`class_id` BIGINT( 22 ) UNSIGNED NOT null COMMENT 'идентификатор класса' ,
	`teacher_id` BIGINT( 22 ) UNSIGNED NOT null COMMENT 'идентификатор преподавателя' ,

	PRIMARY KEY( `class_id` , `teacher_id` ) ,
	CONSTRAINT `fk_class_teacher_class_id` FOREIGN KEY ( `class_id` ) REFERENCES `class`( `id` ) ON DELETE CASCADE ON UPDATE CASCADE ,
	CONSTRAINT `fk_class_teacher_teacher_id` FOREIGN KEY ( `teacher_id` ) REFERENCES `teacher`( `id` ) ON DELETE CASCADE ON UPDATE CASCADE
) COMMENT 'преподаватель преподаёт в классе' ;